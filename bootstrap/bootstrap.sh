#!/bin/sh

apt-get update
apt-get install -q -y --force-yes apache2 apache2-mpm-itk memcached php5-memcache libapache2-mod-php5 php5-mysql vim
rm -rf /var/www
ln -fs /vagrant /var/www

INSTALLER_LOG=/var/log/non-interactive-installer.log

installnoninteractive(){
  sudo bash -c "DEBIAN_FRONTEND=noninteractive aptitude install -q -y $* >> $INSTALLER_LOG"
}
echo 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2' | debconf-set-selections
installnoninteractive mysql-server phpmyadmin

# allow login to phpmyadmin without password
sed -i "s/\/\/\ \$cfg\['Servers'\]\[\$i\]\['AllowNoPassword'\]\ =\ TRUE;/\$cfg\['Servers'\]\[\$i\]\['AllowNoPassword'\]\ =\ TRUE;/" /etc/phpmyadmin/config.inc.php

# change AllowOverride to all
sed -i "s/AllowOverride\ None/AllowOverride\ All/" /etc/apache2/sites-enabled/000-default

# use vagrant user for default host
sed -i "s/DocumentRoot\ \/var\/www/DocumentRoot\ \/var\/www\n\tAssignUserId vagrant vagrant\n/" /etc/apache2/sites-enabled/000-default

mysql -uroot -e 'CREATE DATABASE db'
cat /vagrant/bootstrap/db.sql.gz | gzip -d | mysql -uroot db

sudo a2enmod rewrite

sudo service apache2 restart
