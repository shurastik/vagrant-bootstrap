# Usage

1. Put all files to the project root
2. Put gzipped database dump to bootstrap/db.sql.gz
3. vagrant up

# Result

* Host http://127.0.0.1:8080
* Phpmyadmin: http://127.0.0.1:8080/phpmyadmin/
* MySQL Access - root without password, dump will be restored to database "db"
